#!/bin/bash
#
# This script will automagically download and install Calibre
# from: https://calibre-ebook.com/download_linux
#
# Run with sudo to install system-wide:
# 1. run without any parameters to download and install with confirmation.
# 2. pass parameter: -y in order to auto-upgrade when a new version is found.
# 3. pass parameter: -x to uninstall the app
#
# Depends on: python3, xdg-utils, wget, xz-utils, sed, awk, egrep, getconf, curl, wget, test, tr, trash-cli
#

appname=calibre
appname_proper=$(echo ${appname} | sed -E 's/^(.)(.*)$/\u\1\2/')

allargs=("$@")
numargs=$#

doUninstall=false
optAutoUpgrade=false
exitStatusSuccess=0
exitStatusFail=1
deleteToTrash=false

baseUrl=https://calibre-ebook.com
urlLatest=${baseUrl}/download_linux

tempdlfolder=/tmp/calibre-installer-cache

function printUsage() {
  echo
  echo "-------------------------------------------------------"
  echo "This script \"$(basename $0)\" will download and install ${appname_proper} from ${baseUrl}."
  echo "Run with sudo to install system-wide."
  echo "Parameters:"
  echo " -y : optional, exclusive, will auto-upgrade when a new version is found"
  echo " -x : optional, exclusive, will uninstall the app"
  echo
  echo "When no params passed, will ask for confirmation before installation."
  echo
  echo "Examples:"
  echo "\$ $(basename $0)"
  echo "\$ $(basename $0) -y"
  echo "\$ $(basename $0) -x"
  echo "-------------------------------------------------------"
  echo
}

function pause() {
  read -p "$*"
}

function fileExists() {
  test -e "$1" || test -L "$1"
  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -eq 0 ]; then
    echo "true"
  else
    echo "false"
  fi
}

function setSymlink() {
  # param 1 is "what"
  # param 2 is "where"
  # as in: ln -s what where...

  echo -n "Symlinking \"$1\" to \"$2\"..."
  rm -f "$2" &>/dev/null
  #pause "after removing $2..."
  ln -s "$1" "$2" &>/dev/null
  echo " done"
  #pause "after restoring $2 to $1..."
}

function getTrashMsg() {
  local trashMsg=
  if [[ "${deleteToTrash}" == "true" ]]; then
    trashMsg=' to trash'
  fi
  echo "${trashMsg}"
}

function deleteFile() {
  test -e "$1" || test -L "$1"
  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -eq 0 ]; then
    # the file or symlink exists
    local trashMsg=$(getTrashMsg)
    echo -n "Removing${trashMsg}: $1..."
    if [[ "${deleteToTrash}" == "true" ]]; then
      trash-put "$1" &>/dev/null
    else
      rm -f "$1" &>/dev/null
    fi
    echo " done."
  fi
}

function deleteFolder() {
  if [[ -d "$1" ]]; then
    local trashMsg=$(getTrashMsg)
    echo -n "Removing${trashMsg}: $1..."
    if [[ "${deleteToTrash}" == "true" ]]; then
      trash-put "$1" &>/dev/null
    else
      rm -rf "$1" &>/dev/null
    fi
    echo " done."
  fi
}

function cleanup() {
  echo "Cleanup..."

  deleteFolder "${tempdlfolder}"

  if [[ -d "${initialDir}" ]]; then
    cd "${initialDir}"
  fi
  echo "Cleanup done."
}

function getEffectiveUrl() {
  local initialUrl=$1
  # see where the url is forwarded:
  local fwdurl=$(curl -o /dev/null -w "%{url_effective}\n" -I -L -s -S ${initialUrl})
  echo "${fwdurl}"
}

function handleExitStatus() {
  # arg1: status code; arg2: message
  local EXIT_STATUS=$1
  if [ ${EXIT_STATUS} -ne 0 ]; then
    # failure
    local ERR_MSG=$2
    if [[ -z "${ERR_MSG}" ]]; then
      ERR_MSG="${appname_proper} installation failed."
    fi
    echo
    echo "${ERR_MSG}"
    cleanup
    echo
    exit ${exitStatusFail}
  fi
}

function scrapeToMem() {
  # param 1: url
  local url=$1

  # get html in memory variable:
  local fwdurl=$(getEffectiveUrl "${url}")
  local scrapeFile=
  scrapeFile=$(wget -qnv --show-progress -O - "${fwdurl}")

  if [[ -z "${scrapeFile}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: Failed to scrape URL: ${url}."
  fi

  echo "${scrapeFile}"
}

function prepare() {
  systemWide=false
  initialDir=${PWD}

  local installVerb=install
  if [[ "${doUninstall}" == "true" ]]; then
    installVerb=uninstall
  fi

  # must have admin rights for system-wide installation
  if [ "$(id -u)" == "0" ]; then
    systemWide=true
    deleteToTrash=false
    installPath=/opt
    echo "Will ${installVerb} system-wide."
  else
    handleExitStatus ${exitStatusFail} "Error: This script must be run with sudo."
  fi

  appInstallDir=${installPath}/${appname}
}

function checkInstallPath() {
  echo -n "Checking if installation path exists: ${installPath}..."
  if [[ ! -d "${installPath}" ]]; then
    echo
    echo "Creating directory: ${installPath}."
    mkdir -p "${installPath}"

    handleExitStatus $? "Failed to create installation folder: ${installPath}."
  else
    echo " it does."
  fi
}

function detectAppVersions() {

  echo "Detecting latest release version from: ${urlLatest} ..."

  scrapeFile=$(scrapeToMem "${urlLatest}")

  if [[ -z "${scrapeFile}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: Cannot scrape remote ${appname_proper} version file."
  fi

  local versionLine=$(echo "${scrapeFile}" | grep -i "The latest release of calibre is" | head -1)
  local sedCommonExpr='.*The\s+latest\s+release\s+of\s+calibre\s+is\s+([0-9\.]+)\..*'
  local sedValidationExpr='/'${sedCommonExpr}'/!{q100};'
  # validate first:
  echo "${versionLine}" | sed -E ${sedValidationExpr}' {s|'${sedCommonExpr}'|\1|gI}' &>/dev/null
  handleExitStatus $? "Failed to detect remote version."

  remotever=$(echo "${versionLine}" | sed -E ${sedValidationExpr}' {s|'${sedCommonExpr}'|\1|gI}')

  # root should not/cannot run command in terminal, so we need to impersonate a normal user:
  local normalUser=$(awk -v val=1000 -F ":" '$3==val{print $1}' /etc/passwd)

  # instead of: `command -v`, `which` could also work...
  if ! [[ -x "$(command -v ${appname})" ]]; then
    localver=NONE
    echo "${appname_proper} is not currently installed"
  else
    localver=$(runuser -l ${normalUser} -c "${appname} --version" | sed -E 's/.*\s+([0-9\.]+).*/\1/gI')
  fi

  local parts=
  # must patch local version as sometimes it is such: calibre (calibre 6.23)
  IFS='.' read -ra parts <<<"${localver}"

  local allparts=
  case ${#parts[@]} in
  "1")
    parts+=(0)
    parts+=(0)
    # re-concatenate with dots:
    allparts=$(echo ${parts[@]})
    localver=$(echo ${allparts// /.})
    ;;
  "2")
    parts+=(0)
    # re-concatenate with dots:
    allparts=$(echo ${parts[@]})
    localver=$(echo ${allparts// /.})
    ;;
  *)
    :
    ;;
  esac

  echo "Remote ${appname_proper} version found: ${remotever}."
  echo "Local  ${appname_proper} version found: ${localver}."
}

function checkArguments() {

  if [ ${numargs} -gt 0 ]; then
    echo "Argument(s) passed: ${allargs[@]}."

    # uninstall is exclusive
    if [[ "${doUninstall}" == "true" ]]; then
      optAutoUpgrade=false
    fi
  fi

  if [[ "${doUninstall}" != "true" ]]; then
    detectAppVersions
  fi
}

function decisionBlock() {

  # if no args, let user decide
  if [ ${numargs} -lt 1 ]; then
    read -p "Install latest version available ${remotever}? [y/N] : " decision

    if [[ ${decision} != [yY]* ]]; then
      handleExitStatus ${exitStatusFail} "User denied installation. Aborting."
    fi
  else
    # some args were passed
    if [[ "${doUninstall}" == "true" ]]; then

      read -p "Uninstall ${appname}? [y/N] : " decision

      if [[ ${decision} != [yY]* ]]; then
        handleExitStatus ${exitStatusFail} "User declined un-installation. Aborting."
      fi

    else
      if [[ "${remotever}" == "${localver}" ]]; then
        echo "Latest version ${remotever} already installed, nothing to do."
        cleanup
        exit ${exitStatusSuccess}
      fi

      if [[ "${optAutoUpgrade}" == "true" ]]; then
        echo "Will upgrade to version: ${remotever}..."
      fi

      checkInstallPath
    fi
  fi

  if [[ "${doUninstall}" == "true" ]]; then
    uninstall
  else
    performInstall
  fi
}

function deployApp() {

  echo "Cleanup destination folder before install..."
  deleteFolder "${appInstallDir}"

  # read -p "Stopped for debug. Continue? : [Y/n] : " decision
  # if [[ ${decision} == [nN]* ]]; then
  #     echo "Script execution stopped."
  #     exit 1
  # fi

  mkdir -p "${appInstallDir}"
  handleExitStatus $? "Failed to create installation folder: ${appInstallDir}."

  echo "Executing ${appname_proper} script:"
  echo '> wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sh /dev/stdin install_dir='"${installPath}"
  wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sh /dev/stdin install_dir="${installPath}"

  handleExitStatus $? "Failed to deploy ${appname_proper}."
  echo "Deployed ${appname_proper}."

}

function uninstall() {
  echo "Uninstalling ${appname}..."
  calibre-uninstall
  deleteFolder "${appInstallDir}"
  echo "...uninstalled successfully."
}

function performInstall() {

  echo "Installing latest ${appname_proper}..."

  deployApp
}

function main() {

  # read -p "Press any key to continue: " decision
  prepare

  checkArguments

  decisionBlock

  cleanup

  echo
  echo "${appname_proper} installation completed!"
  echo

}

############
### main ###
############

# checking arguments cannot be done in a function
# list of arguments expected in the input
optstring=":yx"
while getopts ${optstring} arg; do
  case $arg in
  x)
    doUninstall=true
    echo "Will uninstall ${appname}."
    ;;
  y)
    optAutoUpgrade=true
    echo "Will upgrade if any update is available."
    ;;
  *)
    echo "Unknown argument(s): -${arg}. Exiting."
    printUsage
    exit ${exitStatusFail}
    ;;
  esac
done

main

exit ${exitStatusSuccess}
