#!/bin/bash
#
# This script will automagically download and install the libgourou AppImage from https://indefero.soutade.fr/p/libgourou.
# It can and should be integrated with Thunar custom actions.
#
# The installation is only local for user -- never system-wide.
# 1. run without any parameters to download and install with confirmation.
# 2. pass parameter: -q in order to query whether an update is available.
# 3. pass parameter: -x to uninstall the app
# 4. pass parameter: -y in order to auto-upgrade when a new version is found.
#
# Depends on: jq, sed, awk, tar, sleep, egrep, getconf, curl, wget, sha256sum, test, tr, trash-cli
#

appname=libgourou
appname_proper=${appname}

allargs=("$@")
numargs=$#

doUninstall=false
optQueryUpdate=false
optAutoUpgrade=false
exitStatusSuccess=0
exitStatusFail=1
exitStatusUpdateAvailable=100
deleteToTrash=false

systemWide=
shortcutDir=
installPath=/opt
appInstallDir=
versionFileName="${appname}VERSION"
tempdlfolder=/tmp/##${appname}_temp_dl##
bundle=##${appname}_unknown_filename##
scrapeFile=
shortcuts=(adept_activate adept_loan_mgt acsmdownloader adept_remove)

urlBase="https://indefero.soutade.fr"
urlHome="${urlBase}/p/libgourou"
urlLatest="${urlHome}/downloads/"

function printUsage () {
  echo
  echo "-------------------------------------------------------"
  echo "This script \"$(basename $0)\" will download and install ${appname_proper} from ${urlHome}."
  echo "Run as normal user."
  echo "Parameters:"
  echo " -q : optional, exclusive, will only detect if an update is available"
  echo " -x : optional, exclusive, will uninstall the app"
  echo " -y : optional, exclusive, will auto-upgrade when a new version is found"
  echo
  echo "When no params passed, will ask for confirmation before installation."
  echo
  echo "Examples:"
  echo "# $(basename $0)"
  echo "# $(basename $0) -q"
  echo "# $(basename $0) -x"
  echo "# $(basename $0) -y"
  echo "-------------------------------------------------------"
  echo
}

function pause () {
  read -p "$*"
}

function setSymlink () {
  # param 1 is "what"
  # param 2 is "where"
  # as in: ln -s what where...

  echo -n "Symlinking \"$1\" to \"$2\"..."
  rm -f "$2" &> /dev/null
  #pause "after removing $2..."
  ln -s "$1" "$2" &> /dev/null
  echo " done"
  #pause "after restoring $2 to $1..."
}

function getTrashMsg () {
  local trashMsg=
  if [[ "${deleteToTrash}" == "true" ]]; then
    trashMsg=' to trash'
  fi
  echo "${trashMsg}"
}

function deleteFile () {
  test -e "$1" || test -L "$1"
  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -eq 0 ]; then
    # the file or symlink exists
    local trashMsg=$(getTrashMsg)
    echo -n "Removing${trashMsg}: $1..."
    if [[ "${deleteToTrash}" == "true" ]]; then
      trash-put "$1" &> /dev/null
    else
      rm -f "$1" &> /dev/null
    fi
    echo " done."
  fi
}

function deleteFolder () {
  if [[ -d "$1" ]]; then
    local trashMsg=$(getTrashMsg)
    echo -n "Removing${trashMsg}: $1..."
    if [[ "${deleteToTrash}" == "true" ]]; then
      trash-put "$1" &> /dev/null
    else
      rm -rf "$1" &> /dev/null
    fi
    echo " done."
  fi
}

function cleanup () {
  echo "Cleanup..."
  deleteFolder "${tempdlfolder}"
  cd "${initialDir}"
  echo "Cleanup done."
}

function getEffectiveUrl () {
  local initialUrl=$1
  # see where the url is forwarded:
  local fwdurl=$(curl -o /dev/null -w "%{url_effective}" -I -L -s -S ${initialUrl})
  echo "${fwdurl}"
}

function handleExitStatus () {
  # arg1: status code; arg2: message
  local EXIT_STATUS=$1
  if [ ${EXIT_STATUS} -ne 0 ]; then
    # failure
    local ERR_MSG=$2
    if [[ -z "${ERR_MSG}" ]]; then
      ERR_MSG="${appname_proper} installation failed."
    fi
    echo
    echo "${ERR_MSG}"
    cleanup
    echo
    exit ${exitStatusFail}
  fi
}

function downloadFile () {
  # param 1: url
  # param 2: file path (destination)

  local locUrl="$1"
  local locFilePath="$2"
  deleteFile "${locFilePath}"
  echo "Downloading file: ${locFilePath}..."
  # see where the download is forwarded (cdn):
  local fwdurl=$(getEffectiveUrl "${locUrl}")
  echo "From: ${fwdurl}..."

  wget -qnv --show-progress -O "${locFilePath}" "${fwdurl}"
  if [[ ! -f "${locFilePath}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: Failed to download file: \"${locFilePath}\" from \"${locUrl}\""
  else
    echo " done."
  fi
}

function scrapeToMem () {
  # param 1: url
  local url=$1

  # get html in memory variable:
  local fwdurl=$(getEffectiveUrl "${url}")
  local scrapeFile=
  scrapeFile=$(wget -qnv --show-progress -O - "${fwdurl}")

  if [[ -z "${scrapeFile}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: Failed to scrape URL: ${url}."
  fi

  echo "${scrapeFile}"
}

function deleteShortcuts () {
  for fn in ${shortcuts[@]}; do
    deleteFile "${shortcutDir}/${fn}"
  done
}

function restoreShortcuts () {
  for fn in ${shortcuts[@]}; do
    setSymlink "${appInstallDir}/${fn}" "${shortcutDir}/${fn}"
  done
}

function prepare () {

  initialDir=$(pwd)

  systemWide=false
  shortcutDir=${HOME}/.local/bin
  installPath=${HOME}/.local/lib

  local installVerb=install
  if [[ "${doUninstall}" == "true" ]]; then
    installVerb=uninstall
  elif [[ "${optQueryUpdate}" == "true" ]]; then
    installVerb='query version'
  fi

  # check for system-wide installation -- only install this as normal user.
  if [ "$(id -u)" == "0" ]; then
    handleExitStatus ${exitStatusFail} "Cannot ${installVerb} system-wide. Run as normal user."
  else
    echo "Will ${installVerb} for current user only."
  fi

  appInstallDir="${installPath}/${appname}"
  versionFile="${appInstallDir}/${versionFileName}"
}

function checkInstallPath () {
  echo -n "Checking if installation path exists: ${installPath}..."
  if [[ ! -d "${installPath}" ]]; then
    echo
    echo "Creating directory: ${installPath}."
    mkdir -p "${installPath}"
    handleExitStatus $? "Error: cannot create installation path: ${installPath}."
  else
    echo " it does."
  fi
}

function detectAppVersions () {

  if [[ "${doUninstall}" != "true" ]]; then

    scrapeFile=$(scrapeToMem "${urlLatest}")

    if [[ -z "${scrapeFile}" ]]; then
      handleExitStatus ${exitStatusFail} "Error: Cannot scrape remote ${appname_proper} version file."
    fi

    # find line:
    # <a href="/p/libgourou/downloads/172/">url-v0.8.3.txt</a>
    local versionLine=$(echo "${scrapeFile}" | egrep -i "<a\s+href=\"/p/libgourou/downloads/[0-9]+/\">url-v[0-9\.]+\.txt" | head -1)
    local sedCommonExpr='.*<a\s+href="\/p\/libgourou\/downloads\/[0-9]+\/">url-v([0-9\.]+)\.txt<\/a>.*'
    local sedValidationExpr='/'${sedCommonExpr}'/!{q100};'
    # validate first:
    echo "${versionLine}" | sed -E ${sedValidationExpr}' {s|'${sedCommonExpr}'|\1|gI}' &> /dev/null
    handleExitStatus $? "Error: Failed to detect remote version."

    remotever=$(echo "${versionLine}" | sed -E ${sedValidationExpr}' {s|'${sedCommonExpr}'|\1|gI}')

    localver=NONE

    if [[ ! -f "${versionFile}" ]]; then
      echo "${appname_proper} is not currently installed"
    else
      localver=$(cat ${versionFile} | head -n 1)
      if [ -z "${localver}" ]; then
        localver=NONE
      fi
    fi

    echo "Remote ${appname_proper} version found: ${remotever}."
    echo "Local  ${appname_proper} version found: ${localver}."
  fi
}

function checkArguments () {

  if [ ${numargs} -gt 0 ]; then

    echo "Argument(s) passed: ${allargs[@]}."

    # uninstall is exclusive
    if [[ "${doUninstall}" == "true" ]]; then
      optAutoUpgrade=false
    fi
  fi

  if [[ "${doUninstall}" != "true" ]]; then
    checkArchitecture
  fi
}

function checkArchitecture () {

  # architecture can be read this way (returns 64 for 64-bit):
  # or this way (returns x86_64 for 64-bit):
  # arch=$(uname -m)
  local arch=$(getconf LONG_BIT)

  if [ ${arch} -ne 64 ]; then
    handleExitStatus ${exitStatusFail} "Error: CPU architecture mismatch (not 64-bit), aborting installation."
  fi
}

function decisionBlock () {

  # if no args, let user decide
  if [ ${numargs} -lt 1 ]; then
    read -p "Install latest version available ${remotever}? [y/N] : " decision

    if [[ ${decision} != [yY]* ]]; then
      handleExitStatus ${exitStatusFail} "Error: User denied installation. Aborting."
    fi
  else
    # some args were passed
    if [[ "${doUninstall}" == "true" ]]; then

      read -p "Uninstall ${appname}? [y/N] : " decision

      if [[ ${decision} != [yY]* ]]; then
        handleExitStatus ${exitStatusFail} "Error: User declined un-installation. Aborting."
      fi
    elif [[ "${optQueryUpdate}" == "true" ]]; then
      cleanup
      if [[ "${remotever}" == "${localver}" ]]; then
        echo "${appname_proper} latest version ${remotever} already installed."
        exit ${exitStatusSuccess}
      fi
      echo "${appname_proper} update available: ${remotever}"
      exit ${exitStatusUpdateAvailable}
    else
      if [[ "${remotever}" == "${localver}" ]]; then
        echo "Latest ${appname_proper} version ${remotever} already installed, nothing to do."
        cleanup
        exit ${exitStatusSuccess}
      fi

      if [[ "${optAutoUpgrade}" == "true" ]]; then
        echo "Will upgrade to version: ${remotever}..."
      fi

      checkInstallPath
    fi
  fi

  if [[ "${doUninstall}" == "true" ]]; then
    uninstall
  else
    performInstall
  fi
}

function downloadBundle () {

  # detect bundle final url:
  # find line:
  # <a href="/p/libgourou/downloads/172/">url-v0.8.3.txt</a>
  local versionLine=$(echo "${scrapeFile}" | egrep -i "<a\s+href=\"/p/libgourou/downloads/[0-9]+/\">url-v[0-9\.]+\.txt" | head -1)
  local sedCommonExpr='.*<a\s+href="(\/p\/libgourou\/downloads\/[0-9]+\/)">url-v[0-9\.]+\.txt<\/a>.*'
  local sedValidationExpr='/'${sedCommonExpr}'/!{q100};'
  # validate first:
  echo "${versionLine}" | sed -E ${sedValidationExpr}' {s|'${sedCommonExpr}'|\1|gI}' &> /dev/null
  handleExitStatus $? "Error: Failed to detect bundle release page URL."

  local partUrl=$(echo "${versionLine}" | sed -E ${sedValidationExpr}' {s|'${sedCommonExpr}'|\1|gI}')
  local bundleScrapeUrl="${urlBase}${partUrl}"
  echo "Bundle scrape URL: ${bundleScrapeUrl}"
  local bundleScrape=$(scrapeToMem "${bundleScrapeUrl}")

  local bundleLine=$(echo "${bundleScrape}" | egrep -i 'https://.*?/libgourou_utils-'${remotever}'-x86_64.AppImage.tar.gz' | head -1)
  sedCommonExpr='.*(https:.*?\/libgourou_utils-'${remotever}'-x86_64\.AppImage\.tar\.gz).*'
  sedValidationExpr='/'${sedCommonExpr}'/!{q100};'

  # validate first:
  echo "${bundleLine}" | sed -E ${sedValidationExpr}' {s|'${sedCommonExpr}'|\1|gI}' &> /dev/null
  handleExitStatus $? "Error: Failed to detect bundle download URL."

  local bundleUrl=$(echo "${bundleLine}" | sed -E ${sedValidationExpr}' {s|'${sedCommonExpr}'|\1|gI}')

  # final temp working folder:
  tempdlfolder=$(mktemp -d)

  bundle=libgourou_utils-${remotever}-x86_64.AppImage.tar.gz
  bundlePath="${tempdlfolder}/${bundle}"

  downloadFile "${bundleUrl}" "${bundlePath}"

  local filesize=$(stat -c %s "${bundlePath}")
  if [ ${filesize} -lt 1024000 ]; then
    handleExitStatus ${exitStatusFail} "Error: Downloaded bundle file size (${filesize} bytes) too small for: ${bundlePath}."
  fi
}

function verifyChecksum () {
  # soutade.fr does not provide a checksum file
  :

  # read -p "Stopped for debug. Continue? : [Y/n] : " decision
  # if [[ ${decision} == [nN]* ]]; then
  #   echo "Script execution stopped."
  #   cleanup
  #   exit 1
  # fi
}

function deployApp () {
  # we will be extracting the archive in a folder we want:
  echo "Cleanup destination folder before install..."
  deleteShortcuts
  deleteFolder "${appInstallDir}"
  echo "...done."

  mkdir -p "${appInstallDir}"
  handleExitStatus $? "Failed to create installation folder: ${appInstallDir}."

  cd "${appInstallDir}"
  echo -n "Extracting archive..."
  tar -xvf "${bundlePath}" -C "${appInstallDir}" --strip-components=1 &> /dev/null

  handleExitStatus $? "Failed to extract bundle to: ${appInstallDir}."
  echo " done."

  echo -n "Waiting 2 seconds..."
  sleep 2
  echo " done."

  echo ${remotever} > "${versionFile}"
  restoreShortcuts
}

function uninstall () {
  echo "Uninstalling ${appname}..."

  deleteShortcuts
  deleteFolder "${appInstallDir}"

  echo "...uninstalled successfully."
}

function performInstall () {

  echo "Installing latest ${appname_proper}..."

  downloadBundle
  verifyChecksum
  deployApp
}

function main () {

  # read -p "Press any key to continue: " decision
  prepare

  checkArguments

  detectAppVersions

  decisionBlock

  cleanup

  local msg="${appname_proper} installation completed!"
  echo
  echo "${msg}"
  echo

}

############
### main ###
############

echo "> \"$(basename $0)\" will download and install ${appname_proper} from ${urlHome}."
# checking arguments cannot be done in a function
# list of arguments expected in the input
optstring=":qxy"
while getopts ${optstring} arg; do
  case $arg in
    q)
      optQueryUpdate=true
      echo " Will query for an available update."
      ;;
    x)
      doUninstall=true
      echo "Will uninstall ${appname}."
      ;;
    y)
      optAutoUpgrade=true
      echo " Will upgrade if any update is available."
      ;;
    *)
      echo " Unknown argument(s): -${arg}. Exiting."
      printUsage
      exit ${exitStatusFail}
      ;;
  esac
done

main

exit ${exitStatusSuccess}
