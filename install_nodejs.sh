#!/bin/bash
#
# This script will automagically download and install the nodejs bundle
# from: https://nodejs.org/en/download/
#
# Run with sudo to install system-wide, or without to install locally.
# 1. run without any parameters to download and install with confirmation.
# 2. pass parameter: -y in order to auto-upgrade when a new version is found.
# 3. pass parameter: -x to uninstall the app
# 4. pass parameter: -s to restore symlinks only
#
# Depends on: sed, awk, runuser, tar, sleep, egrep, getconf, curl, wget, sha256sum, test, trash-cli
#

appname=nodejs
appname_proper=NodeJS
execnode=node
execnpm=npm
execnpx=npx

allargs=("$@")
numargs=$#

# obtaining release from json doesn't work yet -- because it's hard to obtain a lts release id...
# urlLatest=https://api.github.com/repos/nodejs/node/releases/latest
urlLatest=https://nodejs.org/en/download/

doUninstall=false
optAutoUpgrade=false
optRestoreSymlinks=false
exitStatusSuccess=0
exitStatusFail=1
deleteToTrash=false

systemWide=
shortcutDir=
installPath=/opt
appInstallDir=
shortcutFile1=
shortcutFile2=
shortcutFile3=

function printUsage () {
  echo
  echo "-------------------------------------------------------"
  echo "This script \"$(basename $0)\" will download and install ${appname_proper} LTS from ${urlLatest}."
  echo "Run with sudo to install system-wide, or without to install locally."
  echo "Parameters:"
  echo " -y : optional, exclusive, will auto-upgrade when a new version is found"
  echo " -x : optional, exclusive, will uninstall the app"
  echo " -s : optional, exclusive, will restore the symlinks only"
  echo
  echo "When no params passed, will ask for confirmation before installation."
  echo
  echo "Examples:"
  echo "\$ $(basename $0)"
  echo "\$ $(basename $0) -y"
  echo "\$ $(basename $0) -x"
  echo "\$ $(basename $0) -s"
  echo "-------------------------------------------------------"
  echo
}

function pause () {
  read -p "$*"
}

function setSymlink () {
  # param 1 is "what"
  # param 2 is "where"
  # as in: ln -s what where...

  echo -n "Symlinking \"$1\" to \"$2\"..."
  rm -f "$2" &> /dev/null
  #pause "after removing $2..."
  ln -s "$1" "$2" &> /dev/null
  echo " done"
  #pause "after restoring $2 to $1..."
}

function getTrashMsg () {
  local trashMsg=
  if [[ "${deleteToTrash}" == "true" ]]; then
    trashMsg=' to trash'
  fi
  echo "${trashMsg}"
}

function deleteFile () {
  test -e "$1" || test -L "$1"
  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -eq 0 ]; then
    # the file or symlink exists
    local trashMsg=$(getTrashMsg)
    echo -n "Removing${trashMsg}: $1..."
    if [[ "${deleteToTrash}" == "true" ]]; then
      trash-put "$1" &> /dev/null
    else
      rm -f "$1" &> /dev/null
    fi
    echo " done."
  fi
}

function deleteFolder () {
  if [[ -d "$1" ]]; then
    local trashMsg=$(getTrashMsg)
    echo -n "Removing${trashMsg}: $1..."
    if [[ "${deleteToTrash}" == "true" ]]; then
      trash-put "$1" &> /dev/null
    else
      rm -rf "$1" &> /dev/null
    fi
    echo " done."
  fi
}

function cleanup () {
  echo "Cleanup..."
  deleteFolder "${tempdlfolder}"
  cd "${initialDir}"
  echo "Cleanup done."
}

function getEffectiveUrl () {
  local initialUrl=$1
  # see where the url is forwarded:
  local fwdurl=$(curl -o /dev/null -w "%{url_effective}" -I -L -s -S ${initialUrl})
  echo "${fwdurl}"
}

function handleExitStatus () {
  # arg1: status code; arg2: message (optional)
  local EXIT_STATUS=$1
  if [ ${EXIT_STATUS} -ne 0 ]; then
    # failure
    local ERR_MSG=$2
    if [[ -z "${ERR_MSG}" ]]; then
      ERR_MSG="${appname_proper} installation failed."
    fi
    echo
    echo "${ERR_MSG}"
    cleanup
    echo
    exit ${exitStatusFail}
  fi
}

function downloadFile () {
  # param 1: url
  # param 2: file path (destination)

  local locUrl="$1"
  local locFilePath="$2"
  deleteFile "${locFilePath}"
  echo "Downloading file: ${locFilePath}..."
  # see where the download is forwarded (cdn):
  local fwdurl=$(getEffectiveUrl "${locUrl}")
  echo "From: ${fwdurl}..."

  wget -qnv --show-progress -O "${locFilePath}" "${fwdurl}"
  if [[ ! -f "${locFilePath}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: Failed to download file: \"${locFilePath}\" from \"${locUrl}\""
  else
    echo " done."
  fi
}

function prepare () {
  systemWide=false
  tempdlfolder=/tmp/##${appname}_temp_dl##
  initialDir=$(pwd)

  sedExprSearch='/<a\s+href="[^"]+node-v[0-9\.]+-linux-x64\.tar\.xz"/Ip'
  sedValidationExpr='/.*<a\s+href="[^"]+node-v[0-9\.]+-linux-x64\.tar\.xz".*/!{q100};'

  shortcutDir=${HOME}/.local/bin
  installPath=${HOME}/.local/lib

  local installVerb=install
  if [[ "${doUninstall}" == "true" ]]; then
    installVerb=uninstall
  fi

  # check for system-wide installation
  if [ "$(id -u)" == "0" ]; then
    systemWide=true
    shortcutDir=/usr/local/bin
    installPath=/opt
    echo "Will ${installVerb} system-wide."
  else
    echo "Will ${installVerb} for current user only."
  fi

  appInstallDir=${installPath}/${appname}
  shortcutFile1=${shortcutDir}/${execnode}
  shortcutFile2=${shortcutDir}/${execnpm}
  shortcutFile3=${shortcutDir}/${execnpx}
}

function checkInstallPath () {
  echo -n "Checking if installation path exists: ${installPath}..."
  if [[ ! -d "${installPath}" ]]; then
    echo " nope."
    echo "Creating directory: ${installPath}."
    mkdir -p "${installPath}"

    local EXIT_STATUS=$?
    if [ ${EXIT_STATUS} -ne 0 ]; then
      # failure
      echo "Failed to create installation folder: ${installPath}."
      cleanup
      exit ${exitStatusFail}
    fi

  else
    echo " it does."
  fi
}

function detectAppVersions () {

  echo "Detecting latest release version from: ${urlLatest} ..."

  tempdlfolder=$(mktemp -d)
  cd "${tempdlfolder}"
  htmlLATEST="${tempdlfolder}/${appname}LATEST.html"
  downloadFile "${urlLatest}" "${htmlLATEST}"

  # look for the LTS Linux Binaries:
  # <a href="https://nodejs.org/dist/v18.15.0/node-v18.15.0-linux-x64.tar.xz">64-bit</a>
  # use exit code 100 in case sed does not find expression:
  sed -n -E "${sedExprSearch}" "${htmlLATEST}" | sed -E "${sedValidationExpr}"' {s|.*<a\s+href.+?node-v([0-9\.]+)-linux-x64\.tar\.xz".*|\1|gI}' &> /dev/null
  handleExitStatus $? "Failed to detect remote version."

  remotever=$(sed -n -E "${sedExprSearch}" "${htmlLATEST}" | sed -E "${sedValidationExpr}"' {s|.*<a\s+href.+?node-v([0-9\.]+)-linux-x64\.tar\.xz".*|\1|gI}')
  localver=UNKNOWN

  local crtBinary=$(which ${execnode})
  if [ -n "${crtBinary}" ]; then
    localver=$(node --version | sed -E 's|v([0-9\.]+)|\1|gI')
  fi

  echo "Remote ${appname_proper} version found: ${remotever}."
  echo "Local  ${appname_proper} version found: ${localver}."
}

function checkArguments () {

  if [ ${numargs} -gt 0 ]; then
    echo "Argument(s) passed: ${allargs[@]}."

    # uninstall is exclusive
    if [[ "${doUninstall}" == "true" ]]; then
      optRestoreSymlinks=false
      optAutoUpgrade=false
    fi

    # if user requested to restore symlinks, it must be exclusive!
    if [[ "${optRestoreSymlinks}" == "true" ]]; then
      optAutoUpgrade=false

      # act on "restore symlinks" option:
      restoreShortcuts
      exit ${exitStatusSuccess}
    fi
  fi

  if [[ "${doUninstall}" != "true" ]]; then
    checkArchitecture
    detectAppVersions
  fi
}

function checkArchitecture () {
  # architecture can be read this way (returns 64 for 64-bit):
  local arch=$(getconf LONG_BIT)
  echo "Architecture found: ${arch}."

  if [ ${arch} -ne 64 ]; then
    handleExitStatus ${exitStatusFail} "Error: CPU architecture mismatch (not 64-bit), aborting installation."
  fi
}

function decisionBlock () {

  # if no args, let user decide
  if [ ${numargs} -lt 1 ]; then
    read -p "Install latest version available ${remotever}? [y/N] : " decision

    if [[ ${decision} != [yY]* ]]; then
      handleExitStatus ${exitStatusFail} "Error: User denied installation. Aborting."
    fi
  else
    # some args were passed
    if [[ "${doUninstall}" == "true" ]]; then

      read -p "Uninstall ${appname_proper}? [y/N] : " decision

      if [[ ${decision} != [yY]* ]]; then
        handleExitStatus ${exitStatusFail} "Error: User declined un-installation. Aborting."
      fi

    else
      if [[ "${remotever}" == "${localver}" ]]; then
        echo "Latest version ${remotever} already installed, nothing to do."
        cleanup
        exit ${exitStatusSuccess}
      fi

      if [[ "${optAutoUpgrade}" == "true" ]]; then
        echo "Will upgrade to version: ${remotever}..."
      fi

      checkInstallPath
    fi
  fi

  if [[ "${doUninstall}" == "true" ]]; then
    uninstall
  else
    performInstall
  fi
}

function downloadBundle () {

  # use exit code 100 in case sed does not find expression, see sedValidationExpr:

  # bundle url validation #1
  sed -n -E "${sedExprSearch}" "${htmlLATEST}" | sed -E "${sedValidationExpr}"' {s|.*<a\s+href="([^"]+node-v[0-9\.]+-linux-x64\.tar\.xz)".*|\1|gI}' &> /dev/null
  handleExitStatus $? "Failed to detect bundle download URL."

  local bundleUrl=$(sed -n -E "${sedExprSearch}" "${htmlLATEST}" | sed -E "${sedValidationExpr}"' {s|.*<a\s+href="([^"]+node-v[0-9\.]+-linux-x64\.tar\.xz)".*|\1|gI}')

  # bundle url validation #2
  EXIT_STATUS=99
  local sedHttpValidationExpr='/.*http.+node-v[0-9\.]+-linux-x64\.tar\.xz.*/!{q100};'
  echo "${bundleUrl}" | sed -E "${sedHttpValidationExpr}"' {s|.*(http.+?node-v[0-9\.]+-linux-x64\.tar\.xz).*|\1|gI}' &> /dev/null
  handleExitStatus $? "Detected bundle download URL is invalid."

  # bundle file name
  bundle=$(echo ${bundleUrl} | sed -E 's|.*http.+(node-v[0-9\.]+-linux-x64\.tar\.xz).*|\1|gI')
  bundlePath="${tempdlfolder}/${bundle}"

  downloadFile "${bundleUrl}" "${bundlePath}"

  local filesize=$(stat -c %s "${bundlePath}")
  if [ ${filesize} -lt 1024000 ]; then
    echo "Error: Downloaded bundle file size (${filesize} bytes) too small for: ${bundlePath}."
    cleanup
    exit ${exitStatusFail}
  fi
}

function verifyChecksum () {
  # download sha256sums file
  local shafile=${tempdlfolder}/${appname}_SHA256SUMS
  local shaurl=https://nodejs.org/dist/v${remotever}/SHASUMS256.txt

  downloadFile "${shaurl}" "${shafile}"

  cd "${tempdlfolder}"
  echo -n "Verifying SHA256 checksum..."
  # logic kept while converting it from original:
  # grep node-v18.15.0-linux-x64.tar.xz SHASUMS256.txt | sha256sum -c --strict -
  local grepline=$(grep "${bundle}" "${appname}_SHA256SUMS" | head -1)
  echo "${grepline}" | sha256sum -c --strict --status

  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -eq 0 ]; then
    echo " Pass."
  else
    echo " Failed!"
    cleanup
    exit ${exitStatusFail}
  fi
}

function deployApp () {

  echo "Cleanup destination folder before install..."
  deleteFile "${shortcutFile1}"
  deleteFile "${shortcutFile2}"
  deleteFile "${shortcutFile3}"
  deleteFolder "${appInstallDir}"
  echo "...done."

  # read -p "Stopped for debug. Continue? : [Y/n] : " decision
  # if [[ ${decision} == [nN]* ]]; then
  #     echo "Script execution stopped."
  #     exit 1
  # fi

  mkdir -p "${appInstallDir}"
  handleExitStatus $? "Failed to create installation folder: ${appInstallDir}."

  echo -n "Extracting archive..."
  tar -xf "${bundle}" -C "${appInstallDir}" --strip-components=1 &> /dev/null
  echo " done."

  echo -n "Waiting 3 seconds..."
  sleep 3
  echo " done."
}

function restoreShortcuts () {
  setSymlink "${appInstallDir}/bin/${execnode}" "${shortcutFile1}"
  setSymlink "${appInstallDir}/bin/${execnpm}"  "${shortcutFile2}"
  setSymlink "${appInstallDir}/bin/${execnpx}"  "${shortcutFile3}"
}

function exportPathUtil () {
  # param 1 is the path to be added to the PATH env variable

  local bashrcFile="${HOME}/.bashrc"

  if [[ "${systemWide}" == "true" ]]; then
    bashrcFile=/etc/bashrc
  fi

  # check if path contains our path already:
  echo Reloading file "${bashrcFile}"...
  # . "${bashrcFile}"
  source "${bashrcFile}"
  echo Exporting environment variables to file "${bashrcFile}"...

  if [[ "${PATH}" == *""$1""* ]]; then
    echo "PATH variable contains \"$1\" already..."
    echo "PATH = ${PATH}"
  else
    echo "Adding \"$1\" to PATH variable..."
    echo "Old PATH: ${PATH}"
    local grepres=$(cat ${bashrcFile} | egrep 'export\s.+'"$1")
    # check length of returned string from grep, so we add only once
    if [ ${#grepres} -lt 3 ]; then
      echo 'export NODEJS_HOME='"$1" >> "${bashrcFile}"
      echo 'export PATH='"$1"':$PATH' >> "${bashrcFile}"
    fi
    source "${bashrcFile}"
    sleep 2
    echo "New PATH: ${PATH}"
  fi
}

# this func is never used (!)
function exportPath () {
  exportPathUtil "${appInstallDir}/bin"
}

function uninstall () {
  echo "Uninstalling ${appname}..."
  deleteFile "${shortcutFile1}"
  deleteFile "${shortcutFile2}"
  deleteFile "${shortcutFile3}"
  deleteFolder "${appInstallDir}"
  echo "...uninstalled successfully."
}

function performInstall () {

  echo "Installing latest ${appname_proper}..."

  downloadBundle
  verifyChecksum
  deployApp
  restoreShortcuts
}

function main () {

  # read -p "Press any key to continue: " decision
  prepare

  checkArguments
  
  decisionBlock

  cleanup

  echo "Installation completed!"
  if [[ "${doUninstall}" != "true" ]]; then
    echo "Run as normal user: >\$ ${execnode} --version"
    echo "Run as normal user: >\$ ${execnpm} --version"
  fi

  echo
}

############
### main ###
############

echo "> \"$(basename $0)\" will download and install ${appname_proper} LTS from ${urlLatest}."
# checking arguments cannot be done in a function
# list of arguments expected in the input
optstring=":sxy"
while getopts ${optstring} arg; do
  case $arg in
    s)
      optRestoreSymlinks=true
      echo "Will only restore symlinks."
      ;;
    x)
      doUninstall=true
      echo "Will uninstall ${appname}."
      ;;
    y)
      optAutoUpgrade=true
      echo "Will upgrade if any update is available."
      ;;
    *)
      echo "Unknown argument(s): -${arg}. Exiting."
      printUsage
      exit ${exitStatusFail}
      ;;
  esac
done

main

exit ${exitStatusSuccess}
