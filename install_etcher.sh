#!/bin/bash
#
# This script will automagically download and install the Balena Etcher bundle
# from: https://github.com/balena-io/etcher/releases/latest
#
# Run with sudo to install system-wide, or without to install locally.
# 1. run without any parameters to download and install with confirmation.
# 2. pass parameter: -y in order to auto-upgrade when a new version is found.
# 3. pass parameter: -x to uninstall the app
#
# Depends on: sed, awk, runuser, tar, sleep, egrep, getconf, curl, wget, sha256sum, test
#

appname=etcher
appname_proper="Balena Etcher"
#appname_proper=$(echo ${appname} | sed -E 's/^(.)(.*)$/\u\1\2/')
iconName=balena-etcher.png
iconNameFinal=${appname}$(echo ${iconName} | sed -E 's/^.+(\.[^\.]+)$/\1/')

allargs=("$@")
numargs=$#

doUninstall=false
optAutoUpgrade=false
exitStatusSuccess=0
exitStatusFail=1
deleteToTrash=false

urlLatest=https://api.github.com/repos/balena-io/etcher/releases/latest
iconSize=512

function printUsage () {
  echo
  echo "-------------------------------------------------------"
  echo "This script \"$(basename $0)\" will download and install ${appname_proper} from github."
  echo "Run with sudo to install system-wide, or without to install locally."
  echo "Parameters:"
  echo " -y : optional, exclusive, will auto-upgrade when a new version is found"
  echo " -x : optional, exclusive, will uninstall the app"
  echo
  echo "When no params passed, will ask for confirmation before installation."
  echo
  echo "Examples:"
  echo "\$ $(basename $0)"
  echo "\$ $(basename $0) -y"
  echo "\$ $(basename $0) -x"
  echo "-------------------------------------------------------"
  echo
}

function pause () {
  read -p "$*"
}

function fileExists () {
  test -e "$1" || test -L "$1"
  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -eq 0 ]; then
    echo "true"
  else
    echo "false"
  fi
}

function setSymlink () {
  # param 1 is "what"
  # param 2 is "where"
  # as in: ln -s what where...

  echo -n "Symlinking \"$1\" to \"$2\"..."
  rm -f "$2" &> /dev/null
  #pause "after removing $2..."
  ln -s "$1" "$2" &> /dev/null
  echo " done"
  #pause "after restoring $2 to $1..."
}

function getTrashMsg () {
  local trashMsg=
  if [[ "${deleteToTrash}" == "true" ]]; then
    trashMsg=' to trash'
  fi
  echo "${trashMsg}"
}

function deleteFile () {
  test -e "$1" || test -L "$1"
  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -eq 0 ]; then
    # the file or symlink exists
    local trashMsg=$(getTrashMsg)
    echo -n "Removing${trashMsg}: $1..."
    if [[ "${deleteToTrash}" == "true" ]]; then
      trash-put "$1" &> /dev/null
    else
      rm -f "$1" &> /dev/null
    fi
    echo " done."
  fi
}

function deleteFolder () {
  if [[ -d "$1" ]]; then
    local trashMsg=$(getTrashMsg)
    echo -n "Removing${trashMsg}: $1..."
    if [[ "${deleteToTrash}" == "true" ]]; then
      trash-put "$1" &> /dev/null
    else
      rm -rf "$1" &> /dev/null
    fi
    echo " done."
  fi
}

function cleanup () {
  echo "Cleanup..."
  deleteFolder "${tempdlfolder}"
  if [[ -d "${initialDir}" ]]; then
    cd "${initialDir}"
  fi
  echo "Cleanup done."
}

function getEffectiveUrl () {
  local initialUrl=$1
  # see where the url is forwarded:
  local fwdurl=$(curl -o /dev/null -w "%{url_effective}\n" -I -L -s -S ${initialUrl})
  echo "${fwdurl}"
}

function handleExitStatus () {
  # arg1: status code; arg2: message
  local EXIT_STATUS=$1
  if [ ${EXIT_STATUS} -ne 0 ]; then
    # failure
    local ERR_MSG=$2
    if [[ -z "${ERR_MSG}" ]]; then
      ERR_MSG="${appname_proper} installation failed."
    fi
    echo
    echo "${ERR_MSG}"
    cleanup
    exit ${exitStatusFail}
  fi
}

function prepare () {
  systemWide=false
  tempdlfolder=/tmp/##${appname}_temp_dl##
  initialDir=${PWD}

  shortcutDir=${HOME}/.local/bin
  installPath=${HOME}/.local/lib
  installDesktopDir=${HOME}/.local/share/applications
  installIconDir=${HOME}/.local/share/icons/hicolor/${iconSize}x${iconSize}/apps

  local installVerb=install
  if [[ "${doUninstall}" == "true" ]]; then
    installVerb=uninstall
  fi

  # check for system-wide installation
  if [ "$(id -u)" == "0" ]; then
    systemWide=true
    shortcutDir=/usr/local/bin
    installPath=/opt
    installDesktopDir=/usr/share/applications
    installIconDir=/usr/share/icons/hicolor/${iconSize}x${iconSize}/apps
    echo "Will ${installVerb} system-wide."
  else
    echo "Will ${installVerb} for current user only."
  fi

  appInstallDir=${installPath}/${appname}
  shortcutFile=${shortcutDir}/${appname}
  desktopFile=${installDesktopDir}/${appname}.desktop
  iconFile=${installIconDir}/${iconNameFinal}
}

function checkInstallPath () {
  echo -n "Checking if installation path exists: ${installPath}..."
  if [[ ! -d "${installPath}" ]]; then
    echo
    echo "Creating directory: ${installPath}."
    mkdir -p "${installPath}"
    handleExitStatus $? "Failed to create installation folder: ${installPath}."
  else
    echo " it does."
  fi
}

function detectAppVersions () {

  echo "Detecting latest release version from: ${urlLatest} ..."

  local fwdurl=$(getEffectiveUrl ${urlLatest})
  latestJson=$(curl -s -S "${fwdurl}" | jq .)

  # look for: { tag_name: v1.18.4 }
  remotever=$(echo "${latestJson}" | jq -r .tag_name | sed -E 's|.*v([\.0-9]+).*|\1|gI')
  localver=UNKNOWN

  # detect local version according to our own desktop file
  # instead of: `command -v`, `which` could also work...
  if ! [[ -x "$(command -v ${appname})" ]] ; then
    echo "${appname_proper} is not currently installed"
  else
    # find the installed desktop file and extract the version from it
    local dtopExists=$(fileExists "${desktopFile}")
    if [[ "${dtopExists}" == "true" ]]; then
      # parse the line: X-AppImage-Version=2.7.0
      localver=$(egrep -i 'X-AppImage-Version=' "${desktopFile}" | sed -E 's|^.+=([0-9\.]+).*$|\1|gI')
    else
      echo "Cannot detect local version of ${appname_proper} (desktop file not found)"
    fi
  fi

  echo "Remote ${appname_proper} version found: ${remotever}."
  echo "Local  ${appname_proper} version found: ${localver}."
}

function checkArguments () {

  if [ ${numargs} -gt 0 ]; then
    echo "Argument(s) passed: ${allargs[@]}."

    # uninstall is exclusive
    if [[ "${doUninstall}" == "true" ]]; then
      optAutoUpgrade=false
    fi
  fi

  if [[ "${doUninstall}" != "true" ]]; then
    detectAppVersions
  fi
}

function decisionBlock () {

  # if no args, let user decide
  if [ ${numargs} -lt 1 ]; then
    read -p "Install latest version available ${remotever}? [y/N] : " decision

    if [[ ${decision} != [yY]* ]]; then
      handleExitStatus ${exitStatusFail} "Error: User denied installation. Aborting."
    fi
  else
    # some args were passed
    if [[ "${doUninstall}" == "true" ]]; then

      read -p "Uninstall ${appname}? [y/N] : " decision

      if [[ ${decision} != [yY]* ]]; then
        handleExitStatus ${exitStatusFail} "Error: User declined un-installation. Aborting."
      fi

    else
      if [[ "${remotever}" == "${localver}" ]]; then
        echo "Latest version ${remotever} already installed, nothing to do."
        cleanup
        exit ${exitStatusSuccess}
      fi

      if [[ "${optAutoUpgrade}" == "true" ]]; then
        echo "Will upgrade to version: ${remotever}..."
      fi

      checkInstallPath
    fi
  fi

  if [[ "${doUninstall}" == "true" ]]; then
    uninstall
  else
    performInstall
  fi
}

function downloadBundle () {

  local appImageJson=$(echo ${latestJson} | jq -r '.assets[] | select(.name|test("x64[.]AppImage"; "ins"))')

  # must find a match
  if [[ -z "${appImageJson}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: Could not find published AppImage bundle."
  fi

  bundle=$(echo ${appImageJson} | jq -r '.name')

  tempdlfolder=$(mktemp -d)
  cd "${tempdlfolder}"
  bundlePath=${tempdlfolder}/${bundle}

  deleteFile "${bundlePath}"

  echo "Downloading bundle: ${bundle}..."

  # see where the download is forwarded (cdn):
  local urlDownloadBundle=$(echo ${appImageJson} | jq -r '.browser_download_url')
  local fwdurl=$(getEffectiveUrl ${urlDownloadBundle})

  echo "From: ${fwdurl}..."

  wget -qnv --show-progress -O "${bundlePath}" ${fwdurl}

  if [[ ! -f "${bundlePath}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: Failed to download bundle: ${bundlePath}."
  else
    echo " done."
  fi

  chmod +x "${bundlePath}"
}

function verifyChecksum () {
  # download sha256sums file
  local testexpr=SHA256SUMS[.]Linux[.]x64[.]txt\$
  local checksumJson=$(echo ${latestJson} | jq -r --arg TEXPR "$testexpr" '.assets[] | select(.name|test($TEXPR; "ins"))')
  local checksumFile=$(echo ${checksumJson} | jq -r '.name')
  local checksumURL=$(echo ${checksumJson} | jq -r '.browser_download_url')
  local checksumPath="${tempdlfolder}/${checksumFile}"

  downloadFile "${checksumURL}" "${checksumPath}"

  cd "${tempdlfolder}"
  echo -n "Verifying SHA256 checksum..."
  # logic kept while converting it from original:
  # grep node-v18.15.0-linux-x64.tar.xz SHASUMS256.txt | sha256sum -c --strict -
  local grepline=$(grep "${bundle}" "${checksumFile}" | head -1)
  echo "${grepline}" | sha256sum -c --strict --status

  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -eq 0 ]; then
    echo " Pass."
  else
    echo " Failed!"
    handleExitStatus ${exitStatusFail} "Checksum verification failed."
  fi
}

function deployApp () {

  echo "Cleanup destination folder before install..."
  deleteFile "${shortcutFile}"
  deleteFolder "${appInstallDir}"
  echo "...done."

  # read -p "Stopped for debug. Continue? : [Y/n] : " decision
  # if [[ ${decision} == [nN]* ]]; then
  #     echo "Script execution stopped."
  #     exit 1
  # fi

  mkdir -p "${appInstallDir}"

  handleExitStatus $? "Failed to create installation folder: ${appInstallDir}."

  cp "${bundlePath}" "${appInstallDir}/"
  handleExitStatus $? "Failed to deploy AppImage."
  echo "AppImage deployed."

  chmod +x "${appInstallDir}/${bundle}"
}

function restoreShortcuts () {
  setSymlink "${appInstallDir}/${bundle}" "${shortcutFile}"
}

function deployIcons () {
  deleteFile "${iconFile}"

  local iconPartialPath="usr/share/icons/hicolor/${iconSize}x${iconSize}/apps"
  cd "${tempdlfolder}"
  # extract icon file from bundle -- 512x512 icon
  # declare var first, otherwise the return exit code will be masked:
  local tempIconPart=
  tempIconPart=$(${bundlePath} --appimage-extract "${iconPartialPath}/${iconName}")
  handleExitStatus $? "Error: Failed to extract icon file: ${iconName}."

  local tempIcon=${tempdlfolder}/${tempIconPart}

  if [[ ! -f "${tempIcon}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: Extracted icon file not found: ${tempIcon}."
  else
    # copy it into final location
    echo -n "Deploying icon file: ${iconNameFinal} into ${installIconDir}/..."
    mkdir -p "${installIconDir}"
    cp "${tempIcon}" "${iconFile}" &> /dev/null

    if [[ ! -f "${iconFile}" ]]; then
      handleExitStatus ${exitStatusFail} "Error: Failed to copy icon file: ${iconFile}."
    fi

    echo " done."
  fi

  if [[ "${systemWide}" == "true" ]]; then
    # also copy the icon to pixmaps folder
    mkdir -p /usr/share/pixmaps/ &> /dev/null
    cp "${tempIcon}" "/usr/share/pixmaps/${iconNameFinal}" &> /dev/null
  fi
}

function deployDesktopFile () {

  deleteFile "${desktopFile}"
  echo -n "Creating desktop file: ${desktopFile}..."

  echo "[Desktop Entry]" > ${desktopFile}
  echo "Version=1.0" >> ${desktopFile}
  echo "Encoding=UTF-8" >> ${desktopFile}
  echo "Name=${appname_proper}" >> ${desktopFile}
  echo "Comment=Create USB installation media from bootable ISOs" >> ${desktopFile}
  echo "GenericName=${appname_proper}" >> ${desktopFile}
  echo "X-GNOME-FullName=${appname_proper}" >> ${desktopFile}
  echo "Exec=${appname}" >> ${desktopFile}
  echo "Terminal=false" >> ${desktopFile}
  echo "Type=Application" >> ${desktopFile}
  echo "Icon=${appname}" >> ${desktopFile}
  echo "X-AppImage-Version=${remotever}" >> ${desktopFile}
  echo "Categories=Utility;Accessories;" >> ${desktopFile}

  echo " done."
}

function uninstall () {
  echo "Uninstalling ${appname}..."
  deleteFolder "${appInstallDir}"
  deleteFile "${shortcutFile}"
  deleteFile "${desktopFile}"
  deleteFile "${iconFile}"
  echo "...uninstalled successfully."
}

function performInstall () {

  echo "Installing latest ${appname_proper}..."

  downloadBundle
  verifyChecksum

  deployApp
  restoreShortcuts
  deployIcons
  deployDesktopFile
}

function main () {

  # read -p "Press any key to continue: " decision
  prepare

  checkArguments

  decisionBlock

  cleanup

  echo
  echo "Installation completed!"
  echo

}

############
### main ###
############

# checking arguments cannot be done in a function
# list of arguments expected in the input
optstring=":yx"
while getopts ${optstring} arg; do
  case $arg in
    x)
      doUninstall=true
      echo "Will uninstall ${appname}."
      ;;
    y)
      optAutoUpgrade=true
      echo "Will upgrade if any update is available."
      ;;
    *)
      echo "Unknown argument(s): -${arg}. Exiting."
      printUsage
      exit ${exitStatusFail}
      ;;
  esac
done

main

exit ${exitStatusSuccess}
