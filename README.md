# install-scripts

Various installation scripts for Debian GNU/Linux (and variants)

All these scripts have been written and tested on Debian XFCE 64-bit.

No warranty is implied, I've tested them to the best of my ability.

--sid