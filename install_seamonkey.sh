#!/bin/bash
#
# This script will automagically download and install the 64-bit SeaMonkey from Mozilla.
# It detects the latest stable version from the web page:
#   https://www.seamonkey-project.org/releases/
# The latest stable bundle is then downloaded from:
#   http://ftp.mozilla.org/pub/seamonkey/releases/N.N.N/linux-x86_64/en-US/
#
# run with sudo.
# 1. run without any parameters to download and install with confirmation.
# 2. pass parameter: -s in order to restore symlinks only.
# 3. pass parameter: -y in order to auto-upgrade when a new version is found.
# 4. pass parameter: -x to uninstall the app
#
# Depends on: sed, awk, runuser, tar, sleep, egrep, getconf, curl, wget, sha512sum, test, trash-cli
#

appname=seamonkey
appname_proper=SeaMonkey

allargs=("$@")
numargs=$#

doUninstall=false
optRestoreSymlinks=false
optAutoUpgrade=false
exitStatusSuccess=0
exitStatusFail=1
deleteToTrash=false

systemWide=
shortcutDir=
installPath=/opt
installDesktopDir=
installIconDir=
appInstallDir=
shortcutFile=
desktopFile=

function printUsage () {
  echo
  echo "-------------------------------------------------------"
  echo "This script \"$(basename $0)\" will download and install ${appname_proper} from Mozilla."
  echo "Run with sudo."
  echo "Parameters:"
  echo " -s : optional, exclusive, will only restore symlink"
  echo " -x : optional, exclusive, will uninstall the app"
  echo " -y : optional, exclusive, will auto-upgrade when a new version is found"
  echo
  echo "When no params passed, will ask for confirmation before installation."
  echo
  echo "Examples:"
  echo "# $(basename $0)"
  echo "# $(basename $0) -s"
  echo "# $(basename $0) -x"
  echo "# $(basename $0) -y"
  echo "-------------------------------------------------------"
  echo
}

function pause () {
  read -p "$*"
}

function setSymlink () {
  # param 1 is "what"
  # param 2 is "where"
  # as in: ln -s what where...

  echo -n "Symlinking \"$1\" to \"$2\"..."
  rm -f "$2" &> /dev/null
  #pause "after removing $2..."
  ln -s "$1" "$2" &> /dev/null
  echo " done"
  #pause "after restoring $2 to $1..."
}

function getTrashMsg () {
  local trashMsg=
  if [[ "${deleteToTrash}" == "true" ]]; then
    trashMsg=' to trash'
  fi
  echo "${trashMsg}"
}

function deleteFile () {
  test -e "$1" || test -L "$1"
  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -eq 0 ]; then
    # the file or symlink exists
    local trashMsg=$(getTrashMsg)
    echo -n "Removing${trashMsg}: $1..."
    if [[ "${deleteToTrash}" == "true" ]]; then
      trash-put "$1" &> /dev/null
    else
      rm -f "$1" &> /dev/null
    fi
    echo " done."
  fi
}

function deleteFolder () {
  if [[ -d "$1" ]]; then
    local trashMsg=$(getTrashMsg)
    echo -n "Removing${trashMsg}: $1..."
    if [[ "${deleteToTrash}" == "true" ]]; then
      trash-put "$1" &> /dev/null
    else
      rm -rf "$1" &> /dev/null
    fi
    echo " done."
  fi
}

function cleanup () {
  echo "Cleanup..."
  deleteFolder "${tempdlfolder}"
  cd "${initialDir}"
  echo "Cleanup done."
}

function getEffectiveUrl () {
  local initialUrl=$1
  # see where the url is forwarded:
  local fwdurl=$(curl -o /dev/null -w "%{url_effective}" -I -L -s -S ${initialUrl})
  echo "${fwdurl}"
}

function handleExitStatus () {
  # arg1: status code; arg2: message
  local EXIT_STATUS=$1
  if [ ${EXIT_STATUS} -ne 0 ]; then
    # failure
    local ERR_MSG=$2
    if [[ -z "${ERR_MSG}" ]]; then
      ERR_MSG="${appname_proper} installation failed."
    fi
    echo
    echo "${ERR_MSG}"
    cleanup
    echo
    exit ${exitStatusFail}
  fi
}

function downloadFile () {
  # param 1: url
  # param 2: file path

  local locUrl="$1"
  local locFilePath="$2"
  deleteFile "${locFilePath}"
  echo "Downloading file: ${locFilePath}..."
  # see where the download is forwarded (cdn):
  local fwdurl=$(getEffectiveUrl "${locUrl}")
  echo "From: ${fwdurl}..."

  wget -qnv --show-progress -O "${locFilePath}" "${fwdurl}"
  if [[ ! -f "${locFilePath}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: Failed to download file: \"${locFilePath}\" from \"${locUrl}\""
  else
    echo " done."
  fi
}

function scrapeToMem () {
  # param 1: url
  local url=$1

  # get html in memory variable:
  local fwdurl=$(getEffectiveUrl "${url}")
  local scrapeFile=
  scrapeFile=$(wget -qnv --show-progress -O - "${fwdurl}")

  if [[ -z "${scrapeFile}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: Failed to scrape URL: ${url}."
  fi

  echo "${scrapeFile}"
}

function restoreSymlinks () {
  setSymlink "$1" "$2"
}

function prepare () {

  initialDir=$(pwd)
  tempdlfolder=/tmp/##ff_temp_dl##
  bundle=##${appname}_unknown_filename##

  # Make sure only root can run this script
  if [ "$(id -u)" != "0" ]; then
    echo "Error: This script must be run as root." 1>&2
    echo
    printUsage
    exit ${exitStatusFail}
  fi

  local installVerb=install
  if [[ "${doUninstall}" == "true" ]]; then
    installVerb=uninstall
  fi

  systemWide=true
  shortcutDir=/usr/local/bin
  installPath=/opt
  installDesktopDir=/usr/share/applications
  installIconDir=/usr/share/icons/hicolor/scalable/apps

  appInstallDir=${installPath}/${appname}
  appInstallBinary=${installPath}/${appname}/${appname}
  shortcutFile=${shortcutDir}/${appname}
  desktopFile=${installDesktopDir}/${appname}.desktop

  echo "Will ${installVerb} system-wide."
}

function checkInstallPath () {
  echo -n "Checking if installation path exists: ${installPath}..."
  if [[ ! -d "${installPath}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: installation path not found: ${installPath}."
  else
    echo " it does."
  fi
}

function detectAppVersions () {

  local versionPageUrl=https://www.seamonkey-project.org/releases/

  echo "Detecting latest ${appname_proper} version from: ${versionPageUrl}..."

  local myhtm=$(scrapeToMem ${versionPageUrl})

  local versionLine=$(echo "${myhtm}" | grep "<td class=\"curVersion\">" | head -1)
  local sedValidationExpr='/.*curVersion\">([^<>]+)<\/td>.*/!{q100};'
  # validate first:
  echo "${versionLine}" | sed -E ${sedValidationExpr}' {s/.*curVersion\">([^<>]+)<\/td>/\1/gI}' &> /dev/null
  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -ne 0 ]; then
    # failure
    remotever=UNKNOWN
    echo "Failed to detect remote version."
    cleanup
    exit ${exitStatusFail}
  fi

  remotever=$(echo "${versionLine}" | sed -E ${sedValidationExpr}' {s/.*curVersion\">([^<>]+)<\/td>/\1/gI}')

  # root cannot run seamonkey command in terminal, so we need to impersonate a normal user:
  local normalUser=$(awk -v val=1000 -F ":" '$3==val{print $1}' /etc/passwd)

  # instead of: `command -v`, `which` could also work...
  if ! [[ -x "$(command -v ${appname})" ]] ; then
    localver=NONE
    echo "${appname_proper} is not currently installed"
  else
    localver=$(runuser -l ${normalUser} -c "${appname} --version" | sed -E 's/.*\s+([0-9\.]+).*/\1/gI')
  fi

  echo "Remote ${appname_proper} version found: ${remotever}."
  echo "Local  ${appname_proper} version found: ${localver}."
}

function checkArguments () {

  if [ ${numargs} -gt 0 ]; then

    echo "Argument(s) passed: ${allargs[@]}."

    # uninstall is exclusive
    if [[ "${doUninstall}" == "true" ]]; then
      optRestoreSymlinks=false
      optAutoUpgrade=false
    fi

    # if user requested to restore symlinks, it must be exclusive!
    if [[ "${optRestoreSymlinks}" == "true" ]]; then
      optAutoUpgrade=false

      # act on "restore symlinks" option:
      restoreShortcuts
      exit ${exitStatusSuccess}
    fi
  fi

  if [[ "${doUninstall}" != "true" ]]; then
    checkArchitecture
    detectAppVersions
  fi
}

function checkArchitecture () {

  # architecture can be read this way (returns 64 for 64-bit):
  # or this way (returns x86_64 for 64-bit):
  # arch=$(uname -m)
  local arch=$(getconf LONG_BIT)

  if [ ${arch} -eq 64 ]; then
    echo "Correct CPU architecture found: ${arch}-bit."
  else
    echo "Error: CPU architecture mismatch (not 64-bit), aborting installation."
    exit ${exitStatusFail}
  fi
}

function decisionBlock () {

  # if no args, let user decide
  if [ ${numargs} -lt 1 ]; then
    read -p "Install latest version available ${remotever}? [y/N] : " decision

    if [[ ${decision} != [yY]* ]]; then
      echo "Error: User denied installation. Aborting."
      cleanup
      exit ${exitStatusFail}
    fi
  else
    # some args were passed
    if [[ "${doUninstall}" == "true" ]]; then

      read -p "Uninstall ${appname}? [y/N] : " decision

      if [[ ${decision} != [yY]* ]]; then
        echo "Error: User declined un-installation. Aborting."
        cleanup
        exit ${exitStatusFail}
      fi

    else
      if [[ "${remotever}" == "${localver}" ]]; then
        echo "Latest version ${remotever} already installed, nothing to do."
        cleanup
        exit ${exitStatusSuccess}
      fi

      if [[ "${optAutoUpgrade}" == "true" ]]; then
        echo "Will upgrade to version: ${remotever}..."
      fi

      checkInstallPath
    fi
  fi

  if [[ "${doUninstall}" == "true" ]]; then
    uninstall
  else
    performInstall
  fi
}

function downloadBundle () {

  # final temp working folder:
  tempdlfolder=$(mktemp -d)

  bundle=${appname}-${remotever}.en-US.linux-x86_64.tar.bz2
  bundlePath=${tempdlfolder}/${bundle}

  deleteFile "${bundlePath}"

  echo "Downloading bundle: ${bundle}..."
  local bundleDownloadUrl="http://archive.mozilla.org/pub/${appname}/releases/${remotever}/linux-x86_64/en-US/${bundle}"
  downloadFile "${bundleDownloadUrl}" "${bundlePath}"

  local filesize=$(stat -c %s "${bundlePath}")
  if [ ${filesize} -lt 1024000 ]; then
    echo "Error: Downloaded bundle file size (${filesize} bytes) too small for: ${bundlePath}."
    cleanup
    exit ${exitStatusFail}
  fi
}

function verifyChecksum () {

  local checksumFile=${tempdlfolder}/${appname}_CHECKSUMS
  local checksumUrl=https://archive.mozilla.org/pub/${appname}/releases/${remotever}/linux-x86_64/en-US/seamonkey-${remotever}.checksums

  downloadFile "${checksumUrl}" "${checksumFile}"

  local shalinesrch=sha512.*?linux-x86_64/en-US/${bundle}
  local sha512local=$(sha512sum "${bundlePath}" | awk '{print $1}')
  local sha512remote=$(egrep -i ${shalinesrch} "${checksumFile}" | awk '{print $1}')

  echo "Local  hash : ${sha512local}"
  echo "Remote hash : ${sha512remote}"

  # read -p "debug stop... " decision

  echo -n "Verifying SHA512 checksum..."
  if [[ "${sha512local}" == "${sha512remote}" ]]; then
    echo " Pass."
  else
    echo " Failed!"
    exit ${exitStatusFail} "Checksum verification failed."
  fi
}

function deployApp () {
  # we will be extracting the archive in a folder we want:
  echo "Cleanup destination folder before install..."
  deleteFile "${shortcutFile}"
  deleteFolder "${appInstallDir}"
  echo "...done."

  # read -p "Stopped for debug. Continue? : [Y/n] : " decision
  # if [[ ${decision} == [nN]* ]]; then
  #     echo "Script execution stopped."
  #     exit 1
  # fi

  mkdir -p "${appInstallDir}"
  handleExitStatus $? "Failed to create installation folder: ${appInstallDir}."

  cd "${appInstallDir}"
  echo -n "Extracting archive..."
  tar -xvjf "${bundlePath}" -C "${appInstallDir}" --strip-components=1 &> /dev/null

  handleExitStatus $? "Failed to extract bundle to: ${appInstallDir}."

  echo " done."

  echo -n "Waiting 3 seconds..."
  sleep 3
  echo " done."
}

function restoreShortcuts () {
  echo "Restoring symlinks..."
  restoreSymlinks "${appInstallBinary}" "${shortcutFile}"
  echo "Symlinks restored."
}

function deployDesktopFile () {
  deleteFile "${desktopFile}"

  echo -n "Creating desktop file: ${desktopFile}..."

  echo "[Desktop Entry]" > ${desktopFile}
  handleExitStatus $? "Failed to create desktop file: ${desktopFile}."

  echo "Version=1.0" >> ${desktopFile}
  echo "Encoding=UTF-8" >> ${desktopFile}
  echo "Name=${appname_proper}" >> ${desktopFile}
  echo "Comment=All-In-One Internet Application Suite" >> ${desktopFile}
  echo "GenericName=Web Browser" >> ${desktopFile}
  echo "X-GNOME-FullName=Mozilla ${appname_proper} Internet Application Suite" >> ${desktopFile}
  echo "Exec=${appname} %u" >> ${desktopFile}
  echo "Terminal=false" >> ${desktopFile}
  echo "Type=Application" >> ${desktopFile}
  echo "Icon=${appname}" >> ${desktopFile}
  echo "X-AppImage-Version=${remotever}" >> ${desktopFile}
  echo "Categories=Network;WebBrowser;" >> ${desktopFile}
  echo "MimeType=text/html;text/xml;application/xhtml+xml;application/xml;application/vnd.mozilla.xul+xml;application/rss+xml;application/rdf+xml;x-scheme-handler/http;x-scheme-handler/https;" >> ${desktopFile}
  echo "Actions=new-window;new-private-window;new-tab;mail-view;news-view" >> ${desktopFile}
  echo >> ${desktopFile}
  echo "[Desktop Action new-window]" >> ${desktopFile}
  echo "Name=New Window" >> ${desktopFile}
  echo "Exec=${appname} -new-window" >> ${desktopFile}
  echo >> ${desktopFile}
  echo "[Desktop Action new-private-window]" >> ${desktopFile}
  echo "Name=New Private Window" >> ${desktopFile}
  echo "Exec=${appname} -private" >> ${desktopFile}
  echo >> ${desktopFile}
  echo "[Desktop Action new-tab]" >> ${desktopFile}
  echo "Name=New Tab" >> ${desktopFile}
  # seamonkey does not have about:newtab, so we have to improvise:
  echo "Exec=${appname} -new-tab https://start.duckduckgo.com/" >> ${desktopFile}
  echo >> ${desktopFile}
  echo "[Desktop Action mail-view]" >> ${desktopFile}
  echo "Name=Mail" >> ${desktopFile}
  echo "Exec=${appname} -mail" >> ${desktopFile}
  echo >> ${desktopFile}
  echo "[Desktop Action news-view]" >> ${desktopFile}
  echo "Name=News" >> ${desktopFile}
  echo "Exec=${appname} -news" >> ${desktopFile}

  echo " done."
}

function uninstall () {
  echo "Uninstalling ${appname}..."
  deleteFolder "${appInstallDir}"
  deleteFile "${shortcutFile}"
  deleteFile "${desktopFile}"
  echo "...uninstalled successfully."
}

function performInstall () {

  echo "Installing latest ${appname_proper}..."

  downloadBundle
  verifyChecksum
  deployApp
  restoreShortcuts
  deployDesktopFile
}

function main () {

  # read -p "Press any key to continue: " decision
  prepare

  checkArguments

  decisionBlock

  cleanup

  local msg="${appname_proper} Installation completed!"
  if [[ "${doUninstall}" != "true" ]]; then
    msg="${msg} Run as normal user: >\$ ${appname} --version"
  fi
  echo
  echo "${msg}"
  echo

}

############
### main ###
############

# checking arguments cannot be done in a function
# list of arguments expected in the input
optstring=":sxy"
while getopts ${optstring} arg; do
  case $arg in
    s)
      optRestoreSymlinks=true
      echo " Will only restore symlinks."
      ;;
    x)
      doUninstall=true
      echo "Will uninstall ${appname}."
      ;;
    y)
      optAutoUpgrade=true
      echo " Will upgrade if any update is available."
      ;;
    *)
      echo " Unknown argument(s): -${arg}. Exiting."
      printUsage
      exit ${exitStatusFail}
      ;;
  esac
done

main

exit ${exitStatusSuccess}
