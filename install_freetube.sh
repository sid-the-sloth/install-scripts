#!/bin/bash
#
# This script will automagically download and install the Freetube appimage
# from: https://github.com/FreeTubeApp/FreeTube/releases
#
# Run with sudo to install system-wide, or without to install locally.
# 1. run without any parameters to download and install with confirmation.
# 2. pass parameter: -y in order to auto-upgrade when a new version is found.
# 3. pass parameter: -x to uninstall the app
#
# Depends on: jq, sed, awk, tar, sleep, egrep, getconf, curl, wget, sha256sum, test, tr, trash-cli
#

appname=freetube
appname_proper=FreeTube
#appname_proper=$(echo ${appname} | sed -E 's/^(.)(.*)$/\u\1\2/')
iconName=${appname}.svg
# iconName=$(echo ${appname_proper}.png | tr '[:upper:]' '[:lower:]')

allargs=("$@")
numargs=$#

doUninstall=false
optQueryUpdate=false
optAutoUpgrade=false
exitStatusSuccess=0
exitStatusFail=1
exitStatusUpdateAvailable=100
deleteToTrash=true

urlLatest=https://api.github.com/repos/FreeTubeApp/FreeTube/releases
iconSize=512
iconSubFolder=scalable # could also be: scalable or 256x256 or 512x512

function printUsage () {
  echo
  echo "-------------------------------------------------------"
  echo "This script \"$(basename $0)\" will download and install ${appname_proper} from github."
  echo "Run with sudo to install system-wide, or without to install locally."
  echo "Parameters:"
  echo " -y : optional, exclusive, will auto-upgrade when a new version is found"
  echo " -x : optional, exclusive, will uninstall the app"
  echo
  echo "When no params passed, will ask for confirmation before installation."
  echo
  echo "Examples:"
  echo "\$ $(basename $0)"
  echo "\$ $(basename $0) -y"
  echo "\$ $(basename $0) -x"
  echo "-------------------------------------------------------"
  echo
}

function pause () {
  read -p "$*"
}

function fileExists () {
  test -e "$1" || test -L "$1"
  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -eq 0 ]; then
    echo "true"
  else
    echo "false"
  fi
}

function setSymlink () {
  # param 1 is "what"
  # param 2 is "where"
  # as in: ln -s what where...

  echo -n "Symlinking \"$1\" to \"$2\"..."
  rm -f "$2" &> /dev/null
  #pause "after removing $2..."
  ln -s "$1" "$2" &> /dev/null
  echo " done"
  #pause "after restoring $2 to $1..."
}

function getTrashMsg () {
  local trashMsg=
  if [[ "${deleteToTrash}" == "true" ]]; then
    trashMsg=' to trash'
  fi
  echo "${trashMsg}"
}

function deleteFile () {
  test -e "$1" || test -L "$1"
  local EXIT_STATUS=$?
  if [ ${EXIT_STATUS} -eq 0 ]; then
    # the file or symlink exists
    local trashMsg=$(getTrashMsg)
    echo -n "Removing${trashMsg}: $1..."
    if [[ "${deleteToTrash}" == "true" ]]; then
      trash-put "$1" &> /dev/null
    else
      rm -f "$1" &> /dev/null
    fi
    echo " done."
  fi
}

function deleteFolder () {
  if [[ -d "$1" ]]; then
    local trashMsg=$(getTrashMsg)
    echo -n "Removing${trashMsg}: $1..."
    if [[ "${deleteToTrash}" == "true" ]]; then
      trash-put "$1" &> /dev/null
    else
      rm -rf "$1" &> /dev/null
    fi
    echo " done."
  fi
}

function cleanup () {
  echo "Cleanup..."
  deleteFolder "${tempdlfolder}"
  if [[ -d "${initialDir}" ]]; then
    cd "${initialDir}"
  fi
  echo "Cleanup done."
}

function getEffectiveUrl () {
  local initialUrl=$1
  # see where the url is forwarded:
  local fwdurl=$(curl -o /dev/null -w "%{url_effective}\n" -I -L -s -S ${initialUrl})
  echo "${fwdurl}"
}

function handleExitStatus () {
  # arg1: status code; arg2: message
  local EXIT_STATUS=$1
  if [ ${EXIT_STATUS} -ne 0 ]; then
    # failure
    local ERR_MSG=$2
    if [[ -z "${ERR_MSG}" ]]; then
      ERR_MSG="${appname_proper} installation failed."
    fi
    echo
    echo "${ERR_MSG}"
    cleanup
    echo
    exit ${exitStatusFail}
  fi
}

function downloadFile () {
  # param 1: url
  # param 2: file path (destination)

  local locUrl="$1"
  local locFilePath="$2"
  deleteFile "${locFilePath}"
  echo "Downloading file: ${locFilePath}..."
  # see where the download is forwarded (cdn):
  local fwdurl=$(getEffectiveUrl "${locUrl}")
  echo "From: ${fwdurl}..."

  wget -qnv --show-progress -O "${locFilePath}" "${fwdurl}"
  if [[ ! -f "${locFilePath}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: Failed to download file: \"${locFilePath}\" from \"${locUrl}\""
  else
    echo " done."
  fi
}

function prepare () {
  systemWide=false
  tempdlfolder=/tmp/##${appname}_temp_dl##
  initialDir=${PWD}

  shortcutDir=${HOME}/.local/bin
  installPath=${HOME}/.local/lib
  installDesktopDir=${HOME}/.local/share/applications
  installIconDir=${HOME}/.local/share/icons/hicolor/${iconSubFolder}/apps

  local installVerb=install
  if [[ "${doUninstall}" == "true" ]]; then
    installVerb=uninstall
  elif [[ "${optQueryUpdate}" == "true" ]]; then
    installVerb='query version'
  fi

  # check for system-wide installation
  if [ "$(id -u)" == "0" ]; then
    handleExitStatus ${exitStatusFail} "Cannot ${installVerb} system-wide. Run as normal user."
  else
    echo "Will ${installVerb} for current user only."
  fi

  appInstallDir=${installPath}/${appname}
  shortcutFile=${shortcutDir}/${appname}
  desktopFile=${installDesktopDir}/${appname}.desktop
  iconFile=${installIconDir}/${iconName}
}

function checkInstallPath () {
  echo -n "Checking if installation path exists: ${installPath}..."
  if [[ ! -d "${installPath}" ]]; then
    echo
    echo "Creating directory: ${installPath}."
    mkdir -p "${installPath}"
    handleExitStatus $? "Error: cannot create installation path: ${installPath}."
  else
    echo " it does."
  fi
}

function detectAppVersions () {

  if [[ "${doUninstall}" != "true" ]]; then

    echo "Detecting latest release version from: ${urlLatest} ..."

    local fwdurl=$(getEffectiveUrl ${urlLatest})
    latestJson=$(curl -s -S "${fwdurl}" | jq -r .[0] | jq .)

    # look for: element [0] { tag_name: v0.19.0-beta }
    local versionLine=$(echo "${latestJson}" | jq -r .tag_name)
    local sedCommonExpr='.*v([0-9\.]+[a-z\-]*)$'
    local sedValidationExpr='/'${sedCommonExpr}'/!{q100};'
    # validate first:
    echo "${versionLine}" | sed -E ${sedValidationExpr}' {s|'${sedCommonExpr}'|\1|gI}' &> /dev/null
    handleExitStatus $? "Error: Failed to detect remote version."

    remotever=$(echo "${versionLine}" | sed -E ${sedValidationExpr}' {s|'${sedCommonExpr}'|\1|gI}')
    localver=UNKNOWN

    # instead of: `command -v`, `which` could also work...
    if ! [[ -x "$(command -v ${appname})" ]] ; then
      echo "${appname_proper} is not currently installed"
    else
      # find the installed desktop file and extract the version from it
      local dtopExists=$(fileExists "${desktopFile}")
      if [[ "${dtopExists}" == "true" ]]; then
        # parse the line: X-AppImage-Version=1.76.2.23074.glibc2.17
        localver=$(grep 'X-AppImage-Version=' "${desktopFile}" | sed -E 's|^.+=([^=]+)$|\1|gI')
      else
        echo "Cannot detect local version of ${appname_proper} (desktop file not found)"
      fi
    fi

    echo "Remote ${appname_proper} version found: ${remotever}."
    echo "Local  ${appname_proper} version found: ${localver}."
  fi
}

function checkArguments () {

  if [ ${numargs} -gt 0 ]; then
    echo "Argument(s) passed: ${allargs[@]}."

    # uninstall is exclusive
    if [[ "${doUninstall}" == "true" ]]; then
      optAutoUpgrade=false
    fi
  fi

  if [[ "${doUninstall}" != "true" ]]; then
    checkArchitecture
  fi
}

function checkArchitecture () {

  # architecture can be read this way (returns 64 for 64-bit):
  # or this way (returns x86_64 for 64-bit):
  # arch=$(uname -m)
  local arch=$(getconf LONG_BIT)

  if [ ${arch} -ne 64 ]; then
    handleExitStatus ${exitStatusFail} "Error: CPU architecture mismatch (not 64-bit), aborting installation."
  fi
}

function decisionBlock () {

  # if no args, let user decide
  if [ ${numargs} -lt 1 ]; then
    read -p "Install latest version available ${remotever}? [y/N] : " decision

    if [[ ${decision} != [yY]* ]]; then
      handleExitStatus ${exitStatusFail} "Error: User denied installation. Aborting."
    fi
  else
    # some args were passed
    if [[ "${doUninstall}" == "true" ]]; then

      read -p "Uninstall ${appname}? [y/N] : " decision

      if [[ ${decision} != [yY]* ]]; then
        handleExitStatus ${exitStatusFail} "Error: User declined un-installation. Aborting."
      fi
    elif [[ "${optQueryUpdate}" == "true" ]]; then
      cleanup
      if [[ "${remotever}" == "${localver}" ]]; then
        echo "${appname_proper} latest version ${remotever} already installed."
        exit ${exitStatusSuccess}
      fi
      echo "${appname_proper} update available: ${remotever}"
      exit ${exitStatusUpdateAvailable}
    else
      if [[ "${remotever}" == "${localver}" ]]; then
        echo "Latest ${appname_proper} version ${remotever} already installed, nothing to do."
        cleanup
        exit ${exitStatusSuccess}
      fi

      if [[ "${optAutoUpgrade}" == "true" ]]; then
        echo "Will upgrade to version: ${remotever}..."
      fi

      checkInstallPath
    fi
  fi

  if [[ "${doUninstall}" == "true" ]]; then
    uninstall
  else
    performInstall
  fi
}

function downloadBundle () {

  local appImageJson=$(echo ${latestJson} | jq -r '.assets[] | select(.name|test("amd64[.]AppImage$"; "ins"))')
  bundle=$(echo ${appImageJson} | jq -r '.name')

  tempdlfolder=$(mktemp -d)
  bundlePath="${tempdlfolder}/${bundle}"

  # see where the download is forwarded (cdn):
  local bundleUrl=$(echo ${appImageJson} | jq -r '.browser_download_url')
  downloadFile "${bundleUrl}" "${bundlePath}"

  chmod +x "${bundlePath}"
}

function verifyChecksum () {
  # download sha256sums file
  :
}

function deployApp () {

  echo "Cleanup destination folder before install..."
  deleteFile "${shortcutFile}"
  deleteFolder "${appInstallDir}"
  echo "...done."

  # read -p "Stopped for debug. Continue? : [Y/n] : " decision
  # if [[ ${decision} == [nN]* ]]; then
  #     echo "Script execution stopped."
  #     exit 1
  # fi

  mkdir -p "${appInstallDir}"
  handleExitStatus $? "Failed to create installation folder: ${appInstallDir}."

  cp "${bundlePath}" "${appInstallDir}/"
  handleExitStatus $? "Failed to deploy AppImage."
  echo "AppImage deployed."

  chmod +x "${appInstallDir}/${bundle}"
}

function restoreShortcuts () {
  setSymlink "${appInstallDir}/${bundle}" "${shortcutFile}"
}

function deployIcons () {
  deleteFile "${iconFile}"

  local iconPartialPath="usr/share/icons/hicolor/${iconSubFolder}"
  cd "${tempdlfolder}"
  # extract icon file from bundle -- 512x512 icon
  # declare var first, otherwise the return exit code will be masked:
  local tempIconPart=
  tempIconPart=$(${bundlePath} --appimage-extract "${iconPartialPath}/${iconName}")
  handleExitStatus $? "Error: Failed to extract icon file: ${iconName}."

  local tempIcon=${tempdlfolder}/${tempIconPart}

  if [[ ! -f "${tempIcon}" ]]; then
    handleExitStatus ${exitStatusFail} "Error: Extracted icon file not found: ${tempIcon}."
  else
    # copy it into final location
    echo -n "Deploying icon file: ${iconName} into ${installIconDir}/..."
    mkdir -p "${installIconDir}"
    cp "${tempIcon}" "${installIconDir}/" &> /dev/null

    if [[ ! -f "${iconFile}" ]]; then
      handleExitStatus ${exitStatusFail} "Error: Failed to copy icon file: ${iconFile}."
    fi

    echo " done."
  fi

  if [[ "${systemWide}" == "true" ]]; then
    # also copy the icon to pixmaps folder
    mkdir -p /usr/share/pixmaps/ &> /dev/null
    cp "${tempIcon}" /usr/share/pixmaps/ &> /dev/null
  fi
}

function deployDesktopFile () {

  deleteFile "${desktopFile}"

  # the desktop file provided in the AppImage is unreliable for us
  echo -n "Creating desktop file: ${desktopFile}..."

  echo "[Desktop Entry]" > ${desktopFile}
  echo "Version=1.0" >> ${desktopFile}
  echo "Encoding=UTF-8" >> ${desktopFile}
  echo "Name=${appname_proper}" >> ${desktopFile}
  echo "Comment=A private YouTube client" >> ${desktopFile}
  echo "GenericName=YouTube Client" >> ${desktopFile}
  echo "X-GNOME-FullName=${appname_proper}" >> ${desktopFile}
  echo "Exec=${appname}" >> ${desktopFile}
  echo "Icon=${iconFile}" >> ${desktopFile}
  echo "Type=Application" >> ${desktopFile}
  echo "StartupNotify=false" >> ${desktopFile}
  # bad, will mess up the plank icon:
  # echo "StartupWMClass=${appname_proper}" >> ${desktopFile}
  echo "Keywords=video;youtube;watch;player" >> ${desktopFile}
  echo "X-AppImage-Version=${remotever}" >> ${desktopFile}
  echo "Categories=Video;AudioVideo;Player;" >> ${desktopFile}
  echo "Terminal=false" >> ${desktopFile}

  echo " done."
}

function uninstall () {
  echo "Uninstalling ${appname}..."
  deleteFile "${shortcutFile}"
  deleteFile "${desktopFile}"
  deleteFile "${iconFile}"
  deleteFolder "${appInstallDir}"
  echo "...uninstalled successfully."
}

function performInstall () {

  echo "Installing latest ${appname_proper}..."

  downloadBundle
  verifyChecksum

  deployApp
  restoreShortcuts
  deployIcons
  deployDesktopFile
}

function main () {

  # read -p "Press any key to continue: " decision
  prepare

  checkArguments

  detectAppVersions

  decisionBlock

  cleanup

  echo
  echo "${appname_proper} installation completed!"
  echo

}

############
### main ###
############

# checking arguments cannot be done in a function
# list of arguments expected in the input
optstring=":qxy"
while getopts ${optstring} arg; do
  case $arg in
    q)
      optQueryUpdate=true
      echo " Will query for an available update."
      ;;
    x)
      doUninstall=true
      echo "Will uninstall ${appname}."
      ;;
    y)
      optAutoUpgrade=true
      echo "Will upgrade if any update is available."
      ;;
    *)
      echo "Unknown argument(s): -${arg}. Exiting."
      printUsage
      exit ${exitStatusFail}
      ;;
  esac
done

main

exit ${exitStatusSuccess}
